## TODO

* Provide a way to make F-Droid a system app on rooted phones

* Handle error codes returned by InstallSystemManager callback (like the
  internal code for PackageInstaller does)

* Show changed permissions when doing an unattended install

* Handle ROM updates in some way to avoid being removed from /system

## Links

* [Original MR](https://gitorious.org/f-droid/fdroidclient/merge_requests/37)
